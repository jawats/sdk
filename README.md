# Meltano SDK for Taps and Targets

The Tap and Target SDKs are the fastest way to build custom data extractors and loaders!
Taps and targets built on the SDK are automatically compliant with the
[Singer Spec](https://hub.meltano.com/singer/spec), the
de-facto open source standard for extract and load pipelines.

## Future-proof extractors and loaders, with less code

On average, developers tell us that they write about 70% less code by using the SDK, which
makes learning the SDK a great investment. Furthermore, as new features and capabilities
are added to the SDK, your taps and targets can always take advantage of the latest
capabilities and bug fixes, simply by updating your SDK dependency to the latest version.

## Documentation

- See our [online documentation](https://sdk.meltano.com) for instructions on how
to get started with the SDK.

## Contributing back to the SDK

- For more information on how to contribute, see our [Contributors Guide](https://sdk.meltano.com/en/latest/CONTRIBUTING.html).
